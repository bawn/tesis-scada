//-exportacion
const functions = require('firebase-functions')
const admin = require('firebase-admin')
const account = require('./account.json')
//inicializacion de admin con account
admin.initializeApp({
    //credenciales
    credential: admin.credential.cert(account),
    //direccion de la base de datos
    databaseURL: `https://${account.project_id}.firebaseio.com`
})
//se exporta el metodo onPanelU para function
exports.onPanelU = functions.database
    //refererencia al arbol /paneles/nonbre del panel
    .ref('/Paneles/{panelId}')
    //cuando se actualice realizara una funcion
    .onUpdate((snapshot, context) => {
        //nombre del panel
        const panel = context.params.panelId
        //datos del prototipo
        const data = snapshot.after.val()
        //fecha y hora desde javascript
        const Fecha = new Date().toLocaleDateString("es-CL", { timeZone: "America/Santiago" })
        const Hora = new Date().toLocaleTimeString("es-CL", { timeZone: "America/Santiago" })

        Date.prototype.getWeek = function () {
            var d = new Date(
                Date.UTC(this.getFullYear(), this.getMonth(), this.getDate())
            );
            var dayNum = d.getUTCDay() || 7;
            d.setUTCDate(d.getUTCDate() + 4 - dayNum);
            var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
            return Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
        };

        const Semana = new Date().getWeek();

        const Mes = new Date().getMonth();
        //datos del prototipo
        const Temperatura_Ambiental = data.Temperatura_Ambiental
        const Humedad_Ambiental = data.Humedad_Ambiental
        const Potencia_Panel = data.Potencia_Panel
        const Temperatura_Panel = data.Temperatura_Panel
        const Voltaje_Panel = data.Voltaje_Panel
        const Irriadiacion = data.irradiancia
        const Precipitacion = data.precipitacion
        //se define la base de datos
        const db = admin.database()
        //referencia de la direccion donde se guardara
        const panelRef = db.ref('Historial').child('Paneles').child(panel)
        //push a la base de datos con los datos rescatados de functions
        return panelRef.push({
            Fecha,
            Semana,
            Mes,
            Hora,
            Potencia_Panel,
            Temperatura_Panel,
            Voltaje_Panel,
            Temperatura_Ambiental,
            Humedad_Ambiental,
            Irriadiacion,
            Precipitacion
        })
    });