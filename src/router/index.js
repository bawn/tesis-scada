import Vue from 'vue'
import VueRouter from 'vue-router'
//Se llama a las view
import LoginView from '../views/login/index.vue'
import IndexView from '../views/Index/index.vue'
import PageNotFound from '../views/PageNotFound/index.vue'
import DetalleSecionHoyView from '../views/Index/DetalleSeccionHoy/index.vue'
import PrototipoHoyView from '../views/Index/DetalleSeccionHoy/PrototipoHoy/index.vue'
import DetalleSecionSemanaView from '../views/Index/DetalleSeccionSemana/index.vue'
import PrototipoSemanaView from '../views/Index/DetalleSeccionSemana/PrototipoSemana/index.vue'
import DetalleSecionMesView from '../views/Index/DetalleSeccionMes/index.vue'
import PrototipoMesView from '../views/Index/DetalleSeccionMes/PrototipoMes/index.vue'
import PrototipoView from '../views/Index/Prototipo/index.vue'

Vue.use(VueRouter)

const routes = [
  //rutas de la pagina web
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/',
    name: 'inicio',
    component: IndexView
  },
  {
    path: '*',
    name: 'noEncontrado',
    component: PageNotFound
  },
  {
    path: '/detalleSecionHoy',
    name: 'detalleSecionHoy',
    component: DetalleSecionHoyView,
    props: true
  },
  {
    path: '/detalleSecionHoy/prototipoHoy',
    name: 'prototipoHoy',
    component: PrototipoHoyView,
    props: true
  },
  {
    path: '/detalleSecionMes',
    name: 'detalleSecionMes',
    component: DetalleSecionMesView,
    props: true
  },
  {
    path: '/detalleSecionMes/prototipoMes',
    name: 'prototipoMes',
    component: PrototipoMesView,
    props: true
  },
  {
    path: '/detalleSecionSemana',
    name: 'detalleSecionSemana',
    component: DetalleSecionSemanaView,
    props: true
  },
  {
    path: '/detalleSecionSemana/prototipoSemana',
    name: 'prototipoSemana',
    component: PrototipoSemanaView,
    props: true
  },
  {
    path: '/prototipo',
    name: 'prototipo',
    component: PrototipoView,
    props: true
  }
]
//modo history para que cuando se aprete atras o adelante vuelva a las paginas
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
