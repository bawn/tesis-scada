import Vue from 'vue'
import Vuex from 'vuex'
//se llama a firebase para utilizar sus metodos
import * as firebase from 'firebase/app'
import router from '../router/index.js'

import VuexPersistence from 'vuex-persist'
import { _ } from 'core-js'

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
  //estados que contendran informacion
  state: {
    user: null,
    sectores: [],
    panelesHoy: [],
    panelesSemana: [],
    panelesMes: [],
    hoy: [],
    semana: [],
    mes: [],
    seccionHoy: [],
    seccionSemana: [],
    seccionMes: [],
    areaHoy: [],
    areaSemana: [],
    areaMes: [],
  },
  //Modificaciones de State
  mutations: {
    setUser(state, payload) {
      state.user = payload
    },
    setCargarSectores(state, payload) {
      state.sectores = payload
    },
    setCargarPanelesHoy(state, payload) {
      state.panelesHoy = payload
    },
    setCargarPanelesSemana(state, payload) {
      state.panelesSemana = payload
    },
    setCargarPanelesMes(state, payload) {
      state.panelesMes = payload
    },
    setCargarHoy(state, payload) {
      state.hoy = payload
    },
    setCargarSemana(state, payload) {
      state.semana = payload
    },
    setCargarMes(state, payload) {
      state.mes = payload
    },
    setCargarSeccionHoy(state, payload) {
      state.seccionHoy = payload
    },
    setCargarSeccionSemana(state, payload) {
      state.seccionSemana = payload
    },
    setCargarSeccionMes(state, payload) {
      state.seccionMes = payload
    },
    setCargarAreaHoy(state, payload) {
      state.areaHoy = payload
    },
    setCargarAreaSemana(state, payload) {
      state.areaSemana = payload
    },
    setCargarAreaMes(state, payload) {
      state.areaMes = payload
    }
  },
  //acciones que se llamaran en toda la aplicacion
  actions: {
    //Accion para iniciar Seccion se espera el commit que llamara a la mutation y los datos con payload
    signUserIn({ commit }, payload) {
      //se llama los metodos de firebase de auth
      firebase
        .auth()
        //metodo para inciar seccion con email y password
        .signInWithEmailAndPassword(payload.email, payload.password)
        //Una vez que lo realice realizara una funcion con lo retornado de firebase
        .then(function (ret) {
          //constanste de nuevo usuario logeado
          const newUser = ret.user
          //set del usuario nuevo en el state llamando a la mutation
          commit('setUser', newUser)
        })
        //si hay un error que lo muestre por consola
        .catch(error => {
          console.log(error)
        })
    },
    signUserOut({ commit }) {
      commit('setUser', null)
    },
    //redireccion automatica si no esta logeado
    redirect() {
      if (this.state.user === null || this.state.user === undefined) {
        return router.push({ name: "login" });
      }
    },
    //sectores de 
    cargarSectores({ commit, getters }) {
      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((data) => {
          const sectores = []
          const areas = []
          const prototipos = []

          const obj = data.val()

          for (let key in obj) {
            for (const subKey in obj[key].areas) {
              if(subKey != "titulo"){
                for (let subSubKey in obj[key].areas[subKey].prototipos) {
                  prototipos.push({
                    id: subSubKey,
                    titulo: obj[key].areas[subKey].prototipos[subSubKey].titulo
                  })
                }
                areas.push({
                  id: subKey,
                  titulo: obj[key].areas[subKey].titulo,
                  prototipos: prototipos
                })
              }
            }
            sectores.push({
              id: key,
              titulo: obj[key].titulo,
              areas: areas
            })
          }

          commit('setCargarSectores', sectores)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    cargarPanelesHoy({ commit, getters }, payload) {
      let prototipos = []

      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {
          let user = snapshotUser.val()
          for (let key in user) {
            if (user[key].titulo === payload) {
              for (let subKey in user[key].areas) {
                for (let subSubKey in user[key].areas[subKey].prototipos) {
                  prototipos.push({ Area: user[key].areas[subKey].titulo, Nombre: user[key].areas[subKey].prototipos[subSubKey].titulo })
                  firebase.database().ref('Paneles').on('value', function (snapshot) {
                    const data = snapshot.val()

                    let busqueda = []
                    
                    for (let i in prototipos) {
                      for (let o in data) {

                        if (prototipos[i].Nombre === o) {
                          const prototipo = { Nombre: user[key].areas[subKey].titulo, Area: { titulo: prototipos[i].Nombre, prototipo: data[o] } }

                          busqueda.push(prototipo)
                        }
                      }
                    }
                    commit('setCargarPanelesHoy', busqueda)
                  })
                }
              }
            }
          }
        })

    },
    cargarPanelesSemana({ commit, getters }, payload) {
      Date.prototype.getWeek = function () {
        var d = new Date(
          Date.UTC(this.getFullYear(), this.getMonth(), this.getDate())
        );
        var dayNum = d.getUTCDay() || 7;
        d.setUTCDate(d.getUTCDate() + 4 - dayNum);
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
        return Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
      };
      const Semana = new Date().getWeek();

      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {
          let user = snapshotUser.val()
          for (let key in user) {
            if (user[key].titulo === payload) {
              for (let subKey in user[key].areas) {
                for (let subSubKey in user[key].areas[subKey].prototipos) {
                  firebase.database().ref('Historial').child('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshot) {
                    const data = snapshot.val()

                    let busqueda = []

                    for (let o in data) {
                      if (Semana === data[o].Semana) {
                        const prototipo = {
                          Nombre: user[key].areas[subKey].titulo, Area: {
                            titulo: user[key].areas[subKey].prototipos[subSubKey].titulo, prototipo: {
                              irradiancia: data[o].Irriadiacion,
                              Humedad_Ambiental: data[o].Humedad_Ambiental,
                              Potencia_Panel: data[o].Potencia_Panel,
                              Temperatura_Ambiental: data[o].Temperatura_Ambiental,
                              precipitacion: data[o].Precipitacion,
                              Temperatura_Panel: data[o].Temperatura_Panel,
                              Voltaje_Panel: data[o].Voltaje_Panel,
                            }
                          }
                        }
                        busqueda.push(prototipo)
                      }
                    }

                    commit('setCargarPanelesSemana', busqueda)
                  })
                }
              }
            }
          }
        })
    },
    cargarPanelesMes({ commit, getters }, payload) {
      const Mes = new Date().getMonth();

      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {
          let user = snapshotUser.val()
          for (let key in user) {
            if (user[key].titulo === payload) {
              for (let subKey in user[key].areas) {
                for (let subSubKey in user[key].areas[subKey].prototipos) {
                  firebase.database().ref('Historial').child('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshot) {
                    const data = snapshot.val()

                    let busqueda = []

                    for (let o in data) {
                      if (Mes === data[o].Mes) {
                        const prototipo = {
                          Nombre: user[key].areas[subKey].titulo, Area: {
                            titulo: user[key].areas[subKey].prototipos[subSubKey].titulo, prototipo: {
                              irradiancia: data[o].Irriadiacion,
                              Humedad_Ambiental: data[o].Humedad_Ambiental,
                              Potencia_Panel: data[o].Potencia_Panel,
                              Temperatura_Ambiental: data[o].Temperatura_Ambiental,
                              precipitacion: data[o].Precipitacion,
                              Temperatura_Panel: data[o].Temperatura_Panel,
                              Voltaje_Panel: data[o].Voltaje_Panel,
                            }
                          }
                        }
                        busqueda.push(prototipo)
                      }
                    }

                    commit('setCargarPanelesMes', busqueda)
                  })
                }
              }
            }
          }
        })
    },
    cargarHoy({ commit, getters }) {

      let datos = []

      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {
          let user = snapshotUser.val()

          for (let key in user) {
            for (let subKey in user[key].areas) {
              for (let subSubKey in user[key].areas[subKey].prototipos) {
                firebase.database().ref('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshotPanel) {

                  const data = snapshotPanel.val()

                  datos.push({
                    Humedad_Ambiental: data.Humedad_Ambiental,
                    Potencia_Panel: data.Potencia_Panel,
                    Temperatura_Ambiental: data.Temperatura_Ambiental,
                    Temperatura_Panel: data.Temperatura_Panel,
                    Voltaje_Panel: data.Voltaje_Panel,
                    Precipitacion: data.precipitacion,
                    irradiancia: data.irradiancia
                  })

                })
              }
            }
          }
          
          commit('setCargarHoy', datos)
        })

      datos = []
    },
    cargarSemana({ commit, getters }) {
      Date.prototype.getWeek = function () {
        var d = new Date(
          Date.UTC(this.getFullYear(), this.getMonth(), this.getDate())
        );
        var dayNum = d.getUTCDay() || 7;
        d.setUTCDate(d.getUTCDate() + 4 - dayNum);
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
        return Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
      };
      let datos = []
      const Semana = new Date().getWeek();

      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {

          let user = snapshotUser.val()
          for (let key in user) {
            for (let subKey in user[key].areas) {
              for (let subSubKey in user[key].areas[subKey].prototipos) {
                firebase.database().ref('Historial').child('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshotPanel) {
                  const data = snapshotPanel.val()

                  for (let i in data) {
                    datos.push({
                      Semana: data[i].Semana,
                      Humedad_Ambiental: data[i].Humedad_Ambiental,
                      Potencia_Panel: data[i].Potencia_Panel,
                      Temperatura_Ambiental: data[i].Temperatura_Ambiental,
                      Temperatura_Panel: data[i].Temperatura_Panel,
                      Voltaje_Panel: data[i].Voltaje_Panel,
                      Precipitacion: data[i].Precipitacion,
                      irradiancia: data[i].Irriadiacion
                    })
                  }

                  let busqueda = []

                  for (let i in datos) {
                    if (datos[i].Semana === Semana) {
                      busqueda.push(datos[i])
                    }
                  }
                  datos = []
                  commit('setCargarSemana', busqueda)
                })
              }
            }
          }
        })

    },
    cargarMes({ commit, getters }) {
      let datos = []
      const Mes = new Date().getMonth();

      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {

          let user = snapshotUser.val()
          for (let key in user) {
            for (let subKey in user[key].areas) {
              for (let subSubKey in user[key].areas[subKey].prototipos) {
                firebase.database().ref('Historial').child('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshotPanel) {
                  const data = snapshotPanel.val()

                  for (let i in data) {
                    datos.push({
                      Mes: data[i].Mes,
                      Humedad_Ambiental: data[i].Humedad_Ambiental,
                      Potencia_Panel: data[i].Potencia_Panel,
                      Temperatura_Ambiental: data[i].Temperatura_Ambiental,
                      Temperatura_Panel: data[i].Temperatura_Panel,
                      Voltaje_Panel: data[i].Voltaje_Panel,
                      Precipitacion: data[i].Precipitacion,
                      irradiancia: data[i].Irriadiacion
                    })
                  }

                  let busqueda = []

                  for (let i in datos) {
                    if (datos[i].Mes === Mes) {
                      busqueda.push(datos[i])
                    }
                  }
                  datos = []
                  commit('setCargarMes', busqueda)
                })
              }
            }
          }
        })

    },
    cargarSeccionHoy({ commit, getters }) {

      let datos = []

      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {
          let user = snapshotUser.val()

          for (let key in user) {
            for (let subKey in user[key].areas) {
              for (let subSubKey in user[key].areas[subKey].prototipos) {
                firebase.database().ref('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshotPanel) {

                  const data = snapshotPanel.val()

                  datos.push({
                    Seccion: user[key].titulo,
                    Humedad_Ambiental: data.Humedad_Ambiental,
                    Potencia_Panel: data.Potencia_Panel,
                    Temperatura_Ambiental: data.Temperatura_Ambiental,
                    Temperatura_Panel: data.Temperatura_Panel,
                    Voltaje_Panel: data.Voltaje_Panel,
                    Precipitacion: data.precipitacion,
                    irradiancia: data.irradiancia
                  })
                  commit('setCargarSeccionHoy', datos)

                  datos = []
                })
              }
            }
          }

        })
    },
    cargarSeccionSemana({ commit, getters }) {
      Date.prototype.getWeek = function () {
        var d = new Date(
          Date.UTC(this.getFullYear(), this.getMonth(), this.getDate())
        );
        var dayNum = d.getUTCDay() || 7;
        d.setUTCDate(d.getUTCDate() + 4 - dayNum);
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
        return Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
      };
      let datos = []
      const Semana = new Date().getWeek();

      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {

          let user = snapshotUser.val()
          for (let key in user) {
            for (let subKey in user[key].areas) {
              for (let subSubKey in user[key].areas[subKey].prototipos) {
                firebase.database().ref('Historial').child('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshotPanel) {
                  const data = snapshotPanel.val()

                  for (let i in data) {
                    datos.push({
                      Seccion: user[key].titulo,
                      Semana: data[i].Semana,
                      Humedad_Ambiental: data[i].Humedad_Ambiental,
                      Potencia_Panel: data[i].Potencia_Panel,
                      Temperatura_Ambiental: data[i].Temperatura_Ambiental,
                      Voltaje_Panel: data[i].Voltaje_Panel,
                      Temperatura_Panel: data[i].Temperatura_Panel,
                      Precipitacion: data[i].Precipitacion,
                      irradiancia: data[i].Irriadiacion,
                    })
                  }

                  let busqueda = []

                  for (let i in datos) {
                    if (datos[i].Semana === Semana) {
                      busqueda.push(datos[i])
                    }
                  }

                  datos = []

                  commit('setCargarSeccionSemana', busqueda)
                })
              }
            }
          }
        })

    },
    cargarSeccionMes({ commit, getters }) {
      let datos = []
      const Mes = new Date().getMonth();

      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {

          let user = snapshotUser.val()
          for (let key in user) {
            for (let subKey in user[key].areas) {
              for (let subSubKey in user[key].areas[subKey].prototipos) {
                firebase.database().ref('Historial').child('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshotPanel) {
                  const data = snapshotPanel.val()

                  for (let i in data) {
                    datos.push({
                      Seccion: user[key].titulo,
                      Mes: data[i].Mes,
                      Humedad_Ambiental: data[i].Humedad_Ambiental,
                      Potencia_Panel: data[i].Potencia_Panel,
                      Temperatura_Ambiental: data[i].Temperatura_Ambiental,
                      Voltaje_Panel: data[i].Voltaje_Panel,
                      Temperatura_Panel: data[i].Temperatura_Panel,
                      Precipitacion: data[i].Precipitacion,
                      irradiancia: data[i].Irriadiacion,
                    })
                  }

                  let busqueda = []

                  for (let i in datos) {
                    if (datos[i].Mes === Mes) {
                      busqueda.push(datos[i])
                    }
                  }
                  datos = []
                  commit('setCargarSeccionMes', busqueda)
                })
              }
            }
          }
        })

    },
    cargarAreaHoy({ commit, getters }, payload) {
      let datos = []
      const Fecha = new Date().toLocaleDateString("es-CL", { timeZone: "America/Santiago" })

      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {
          let user = snapshotUser.val()

          for (let key in user) {
            if (user[key].titulo === payload) {
              for (let subKey in user[key].areas) {
                for (let subSubKey in user[key].areas[subKey].prototipos) {
                  firebase.database().ref('Historial').child('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshotPanel) {

                    const data = snapshotPanel.val()

                    for (let i in data) {
                      datos.push({
                        Area: user[key].areas[subKey].titulo,
                        Fecha: new Date(data[i].Fecha).toLocaleDateString("es-CL", { timeZone: "America/Santiago" }),
                        Hora: data[i].Hora,
                        Humedad_Ambiental: data[i].Humedad_Ambiental,
                        Potencia_Panel: data[i].Potencia_Panel,
                        Temperatura_Ambiental: data[i].Temperatura_Ambiental,
                        Voltaje_Panel: data[i].Voltaje_Panel,
                        Temperatura_Panel: data[i].Temperatura_Panel,
                        Precipitacion: data[i].Precipitacion,
                        irradiancia: data[i].Irriadiacion,
                      })
                    }

                    let busqueda = []

                    for (let i in datos) {
                      if (datos[i].Fecha == Fecha) {

                        busqueda.push(datos[i])
                      }
                    }


                    commit('setCargarAreaHoy', busqueda)

                    datos = []
                  })
                }
              }
            }
          }

        })
    },
    cargarAreaSemana({ commit, getters }, payload) {
      Date.prototype.getWeek = function () {
        var d = new Date(
          Date.UTC(this.getFullYear(), this.getMonth(), this.getDate())
        );
        var dayNum = d.getUTCDay() || 7;
        d.setUTCDate(d.getUTCDate() + 4 - dayNum);
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
        return Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
      };
      let datos = []
      const Semana = new Date().getWeek();


      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {
          let user = snapshotUser.val()

          for (let key in user) {
            if (user[key].titulo === payload) {
              for (let subKey in user[key].areas) {
                for (let subSubKey in user[key].areas[subKey].prototipos) {
                  firebase.database().ref('Historial').child('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshotPanel) {

                    const data = snapshotPanel.val()

                    for (let i in data) {
                      datos.push({
                        Area: user[key].areas[subKey].titulo,
                        Semana: data[i].Semana,
                        Hora: data[i].Hora,
                        Humedad_Ambiental: data[i].Humedad_Ambiental,
                        Potencia_Panel: data[i].Potencia_Panel,
                        Temperatura_Ambiental: data[i].Temperatura_Ambiental,
                        Voltaje_Panel: data[i].Voltaje_Panel,
                        Temperatura_Panel: data[i].Temperatura_Panel,
                        Precipitacion: data[i].Precipitacion,
                        irradiancia: data[i].Irriadiacion
                      })
                    }

                    let busqueda = []

                    for (let i in datos) {
                      if (datos[i].Semana === Semana) {
                        busqueda.push(datos[i])
                      }
                    }
                    commit('setCargarAreaSemana', busqueda)

                    datos = []
                  })
                }
              }
            }
          }

        })
    },
    cargarAreaMes({ commit, getters }, payload) {
      let datos = []
      const Mes = new Date().getMonth();
      firebase.database().ref('Usuarios').child(getters.user.uid).once('value')
        .then((snapshotUser) => {
          let user = snapshotUser.val()

          for (let key in user) {
            if (user[key].titulo === payload) {
              for (let subKey in user[key].areas) {
                for (let subSubKey in user[key].areas[subKey].prototipos) {
                  firebase.database().ref('Historial').child('Paneles').child(user[key].areas[subKey].prototipos[subSubKey].titulo).on('value', function (snapshotPanel) {

                    const data = snapshotPanel.val()

                    for (let i in data) {
                      datos.push({
                        Area: user[key].areas[subKey].titulo,
                        Mes: data[i].Mes,
                        Hora: data[i].Hora,
                        Humedad_Ambiental: data[i].Humedad_Ambiental,
                        Potencia_Panel: data[i].Potencia_Panel,
                        Temperatura_Ambiental: data[i].Temperatura_Ambiental,
                        Voltaje_Panel: data[i].Voltaje_Panel,
                        Temperatura_Panel: data[i].Temperatura_Panel,
                        Precipitacion: data[i].Precipitacion,
                        irradiancia: data[i].Irriadiacion,
                      })
                    }

                    let busqueda = []

                    for (let i in datos) {
                      if (datos[i].Mes === Mes) {
                        busqueda.push(datos[i])
                      }
                    }

                    commit('setCargarAreaMes', datos)

                    datos = []
                  })
                }
              }
            }
          }

        })
    },
    crearSector({ getters }, payload) {
      const sector = ({
        titulo: payload
      })

      firebase
        .database()
        .ref('Usuarios')
        .child(getters.user.uid)
        .push(sector)
        .catch((error) => {
          console.log(error)
        })
    },
    crearArea({ getters }, payload) {
      const area = ({
        titulo: payload.titulo
      })
      firebase
        .database()
        .ref('Usuarios')
        .child(getters.user.uid)
        .child(payload.id)
        .child('areas')
        .push(area)
        .catch((error) => {
          console.log(error)
        })
    },
    crearPrototipo({ getters }, payload) {
      const prototipo = ({
        titulo: payload.titulo
      })
      firebase
        .database()
        .ref('Usuarios')
        .child(getters.user.uid)
        .child(payload.idSector)
        .child('areas')
        .child(payload.idArea)
        .child('prototipos')
        .push(prototipo)
        .catch((error) => {
          console.log(error)
        })
    },
    editarSector({ getters }, payload) {
      let obj = {}

      obj.titulo = payload.titulo

      firebase.database().ref('Usuarios').child(getters.user.uid).child(payload.id).update(obj)
        .catch((error) => {
          console.log(error)
        })
    },
    eliminarSector({ getters }, payload) {
      firebase.database().ref(`Usuarios/${getters.user.uid}/${payload}`).remove()
        .then(() => {

        })
        .catch((error) => {
          console.log(error)
        })
    },
    editarArea({ getters }, payload) {
      let obj = {}

      obj.titulo = payload.titulo

      firebase.database().ref('Usuarios').child(getters.user.uid).child(payload.idPrototipo).child('areas').child(payload.idArea).update(obj)
        .catch((error) => {
          console.log(error)
        })
    },
    eliminarArea({ getters }, payload) {
      firebase.database().ref(`Usuarios/${getters.user.uid}/${payload.idSector}/areas/${payload.idArea}`).remove()
        .catch((error) => {
          console.log(error)
        })
    },
    eliminarPrototipo({ getters }, payload) {
      firebase.database().ref(`Usuarios/${getters.user.uid}/${payload.idSector}/areas/${payload.idArea}/prototipos/${payload.idPrototipo}`).remove()
        .catch((error) => {
          console.log(error)
        })
    }
  },
  //modulos que puedan ser utilizados por la aplicacion
  modules: {
  },
  //Variables que se pueden llamar en toda la aplicacion
  getters: {
    user(state) {
      return state.user
    },
    sectores(state) {
      return state.sectores
    },
    panelesHoy(state) {
      return state.panelesHoy
    },
    panelesSemana(state) {
      return state.panelesSemana
    },
    panelesMes(state) {
      return state.panelesMes
    },
    hoy(state) {
      return state.hoy
    },
    semana(state) {
      return state.semana
    },
    mes(state) {
      return state.mes
    },
    seccionHoy(state) {
      return state.seccionHoy
    },
    seccionSemana(state) {
      return state.seccionSemana
    },
    seccionMes(state) {
      return state.seccionMes
    },
    areaHoy(state) {
      return state.areaHoy
    },
    areaSemana(state) {
      return state.areaSemana
    },
    areaMes(state) {
      return state.areaMes
    }
  },
  plugins: [vuexLocal.plugin]
})
