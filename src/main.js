//import default
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//import vuetify
import vuetify from './plugins/vuetify';
//import todo de plugin vue-aweosomefont
import * as icons from './plugins/vue-awesomefont';
//inicializacion firebase
import firebase from 'firebase'

import * as VueFire from 'vuefire'

import * as apexChart from './plugins/apexChart'

Vue.use(VueFire)

Vue.config.productionTip = false

//se indica que todo se utilizara en vue
new Vue({
  router,
  store,
  vuetify,
  VueFire,
  apexChart,
  icons,
  render: h => h(App),
  created(){
    firebase.initializeApp({
      apiKey: "AIzaSyALs_rm2485m1nXw-dX3UhgIkYMrMaN1Dw",
      authDomain: "tesiscada.firebaseapp.com",
      databaseURL: "https://tesiscada.firebaseio.com",
      projectId: "tesiscada",
      storageBucket: "tesiscada.appspot.com",
      messagingSenderId: "1024008204832",
      appId: "1:1024008204832:web:6bc60973672fe390c28245"
    })

    this.$store.dispatch('cargarSectores')
    this.$store.dispatch('cargarHoy')
    this.$store.dispatch('cargarSemana')
    this.$store.dispatch('cargarMes')
    this.$store.dispatch('cargarSeccionHoy')
    this.$store.dispatch('cargarSeccionSemana')
    this.$store.dispatch('cargarSeccionMes')
  }
}).$mount('#app')
