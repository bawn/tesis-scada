import Vue from 'vue';
//import libreria, iconos y vue-fontawesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
//se añade a la libreria los iconos solid, regular y brand
library.add(fas)
library.add(far)
library.add(fab)
//se le indica a vue que utilizara iconos de font awesome y se le une al vue-fontawesome
Vue.component('font-awesome-icon', FontAwesomeIcon)
